#!/bin/bash

set -euxo pipefail

S3_URI=''
CLOUDFRONT_ID=''



function get-infrastructure() {
    pushd aws

    terraform init
    S3_URI="$( terraform output --raw s3_uri )"
    CLOUDFRONT_ID="$( terraform output --raw cloudfront_id )"

    popd
}



function publish() {
    local deployment="$( mktemp --directory --tmpdir=. deployment.XXXXXXXXXX )"

    # git clone sets the modified timestamp on every file in the project to the time of
    # clone, not the time it was committed. aws s3 sync only detects change using modified
    # timestamps, so s3 sync in gitlab CI will always upload every file.
    # use rsync's checksum flag to calcuate what actually changed in content.
    aws s3 sync "$S3_URI" "$deployment" --no-progress
    rsync --checksum --recursive --delete --verbose src/ "$deployment"/

    aws s3 sync "$deployment" "$S3_URI" --delete --no-progress | tee aws-s3-sync.log

    if [[ $( wc -l <aws-s3-sync.log ) -eq 0 ]] ; then
        echo "nothing changed. skipping cloudfront invalidation"
        rm aws-s3-sync.log
        return
    fi

    rm aws-s3-sync.log
    local id="$( aws cloudfront create-invalidation --distribution-id "$CLOUDFRONT_ID" --paths '/*' | jq -r '.Invalidation.Id' )"
    aws cloudfront wait invalidation-completed --distribution-id "$CLOUDFRONT_ID" --id "$id"
}


get-infrastructure
publish

