
variable "name" {
  type = string
  description = "the bucket name"
}

variable "tags" {
  type = map(string)
  description = "tags to be applied to every resource"
}



output "bucket_arn" {
  description = "the ARN of the S3 bucket"
  value = aws_s3_bucket.bucket.arn
}

output "bucket_name" {
  description = "the name of the S3 bucket"
  value = aws_s3_bucket.bucket.id
}

output "bucket_regional_domain_name" {
  description = "the regional domain name of the S3 bucket"
  value = aws_s3_bucket.bucket.bucket_regional_domain_name
}

output "read_write_policy_arn" {
  description = "the IAM policy ARN granting read/write access to the bucket"
  value = aws_iam_policy.read_write_policy.arn
}



resource "aws_s3_bucket" "bucket" {
  bucket = var.name
  acl = "private"

  tags = var.tags
}



data "aws_iam_policy_document" "read_write_access" {
  statement {
    actions = [
      "s3:ListBucket",
    ]

    resources = [
      aws_s3_bucket.bucket.arn,
    ]
  }

  statement {
    actions = [
      "s3:GetObject",
      "s3:PutObject",
      "s3:DeleteObject",
    ]

    resources = [
      "${aws_s3_bucket.bucket.arn}/*",
    ]
  }
}

resource "aws_iam_policy" "read_write_policy" {
  name = "${aws_s3_bucket.bucket.id}-s3-read-write-access"
  description = "grants read/write access to the ${aws_s3_bucket.bucket.id} s3 bucket"
  policy = data.aws_iam_policy_document.read_write_access.json
  tags = var.tags
}

