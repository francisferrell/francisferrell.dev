
variable "name" {
  type = string
  description = "the user name"
}

variable "keybase_user" {
  type = string
  description = "keybase username to use to encrypt the secret_access_key output"
  default = null
}

variable "policy_arns" {
  type = list(string)
  description = "policies that should be attached to the user"
  default = []
}

variable "tags" {
  type = map(string)
  description = "tags to be applied to every resource"
}



output "access_key_id" {
  description = "the user's access key id"
  value = aws_iam_access_key.key.id
}

output "secret_access_key" {
  description = "the user's access key id"
  value = aws_iam_access_key.key.encrypted_secret
}



resource "aws_iam_user" "user" {
  name = var.name
  tags = var.tags
}

resource "aws_iam_access_key" "key" {
  user = aws_iam_user.user.name
  pgp_key = var.keybase_user == null ? "" : "keybase:${var.keybase_user}"
}

resource "aws_iam_user_policy_attachment" "policy_attachments" {
  for_each = toset( var.policy_arns )

  user = aws_iam_user.user.name
  policy_arn = each.value
}

