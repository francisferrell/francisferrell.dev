
variable "bucket_arn" {
  type = string
  description = "the ARN of the S3 bucket"
}

variable "bucket_name" {
  type = string
  description = "the name of the S3 bucket"
}

variable "bucket_regional_domain_name" {
  type = string
  description = "the regional domain name of the S3 bucket"
}

variable "certificate_arn" {
  type = string
  description = "the ARN of the ACM certificate to be used by the cloudfront distribution"
}

variable "default_root_object" {
  type = string
  description = "The object that you want CloudFront to return (for example, index.html) when an end user requests the root URL."
  default = "index.html"
}

variable "domain_name" {
  type = string
  description = "the domain name to be aliased to the cloudfront distribution"
}

variable "s3_root" {
  type = string
  description = "the root path within the S3 bucket that should be hosted by cloudfront. must begin with a / and not end with a /"
  default = ""
}

variable "zone_id" {
  type = string
  description = "the Route 53 zone in which to create the DNS alias"
}

variable "tags" {
  type = map(string)
  description = "tags to be applied to every resource"
}



output "distribution_id" {
  description = "the ID of the cloudfront distribution"
  value = aws_cloudfront_distribution.web.id
}

output "invalidation_policy_arn" {
  description = "the IAM policy ARN granting invalidation access to the distribution"
  value = aws_iam_policy.invalidation_policy.arn
}



locals {
  s3_origin_id = "${var.domain_name}-origin-s3"
}



resource "aws_cloudfront_distribution" "web" {
  wait_for_deployment = false
  enabled = true
  comment = var.domain_name
  is_ipv6_enabled = true
  default_root_object = var.default_root_object
  aliases = [ var.domain_name ]
  price_class = "PriceClass_All"

  viewer_certificate {
    acm_certificate_arn = var.certificate_arn
    minimum_protocol_version = "TLSv1.2_2018"
    ssl_support_method = "sni-only"
  }

  origin {
    domain_name = var.bucket_regional_domain_name
    origin_id = local.s3_origin_id
    origin_path = var.s3_root

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  default_cache_behavior {
    target_origin_id = local.s3_origin_id

    allowed_methods = ["GET", "HEAD"]
    cached_methods = ["GET", "HEAD"]
    viewer_protocol_policy = "redirect-to-https"
    compress = true
    default_ttl = 86400
    max_ttl = 2592000

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = var.tags
}


resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "${var.domain_name} S3 access"
}



data "aws_iam_policy_document" "s3_cloudfront_policy_document" {
  statement {
    actions = [
      "s3:ListBucket",
    ]

    principals {
      type = "AWS"
      identifiers = [
        aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn,
      ]
    }

    resources = [ var.bucket_arn ]
  }

  statement {
    actions = [
      "s3:GetObject",
    ]

    principals {
      type = "AWS"
      identifiers = [
        aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn,
      ]
    }

    resources = [ "${var.bucket_arn}${var.s3_root}/*" ]
  }
}

resource "aws_s3_bucket_policy" "s3_cloudfront_policy" {
  bucket = var.bucket_name
  policy = data.aws_iam_policy_document.s3_cloudfront_policy_document.json
}



resource "aws_route53_record" "dns" {
  for_each = toset( [ "A", "AAAA" ] )

  zone_id = var.zone_id
  name = var.domain_name
  type = each.value

  alias {
    name = aws_cloudfront_distribution.web.domain_name
    zone_id = aws_cloudfront_distribution.web.hosted_zone_id
    evaluate_target_health = false
  }
}



data "aws_iam_policy_document" "invalidation_access" {
  statement {
    actions = [
      "cloudfront:CreateInvalidation",
      "cloudfront:GetInvalidation",
    ]

    resources = [
      aws_cloudfront_distribution.web.arn
    ]
  }
}

resource "aws_iam_policy" "invalidation_policy" {
  name = "${var.domain_name}-cloudfront-invalidation-access"
  description = "grants access to invalidate the ${var.domain_name} cloudfront distribution"
  policy = data.aws_iam_policy_document.invalidation_access.json
  tags = var.tags
}

