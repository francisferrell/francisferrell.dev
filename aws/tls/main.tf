
variable "domain_name" {
  type = string
  description = "the domain name to be covered by the certificate"
}

variable "zone_id" {
  type = string
  description = "the Route 53 zone in which to create the DNS validation records"
}

variable "tags" {
  type = map(string)
  description = "tags to be applied to every resource"
}



output "arn" {
  description = "the ARN of the ACM certificate that is created"
  value = aws_acm_certificate.certificate.arn
}



resource "aws_acm_certificate" "certificate" {
  domain_name = var.domain_name
  validation_method = "DNS"
  tags = var.tags
}



resource "aws_acm_certificate_validation" "certificate_validation" {
  certificate_arn = aws_acm_certificate.certificate.arn
  validation_record_fqdns = [ for record in aws_route53_record.validation_record : record.fqdn ]
}



resource "aws_route53_record" "validation_record" {
  for_each = {
    for dvo in aws_acm_certificate.certificate.domain_validation_options : dvo.domain_name => {
      name = dvo.resource_record_name
      value = dvo.resource_record_value
      type = dvo.resource_record_type
    }
  }

  zone_id = var.zone_id
  name = each.value.name
  records = [ each.value.value ]
  type = each.value.type
  allow_overwrite = true
  ttl = 60
}

