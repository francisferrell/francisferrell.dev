
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.64.0"
    }
  }

  backend "s3" {
    profile = "terraform"
    region = "us-east-1"
    bucket = "terraform-francisferrell"
    key = "francisferrell.dev.tfstate"
    dynamodb_table = "terraform-francisferrell"
  }
}

provider "aws" {
  profile = "terraform"
  region = "us-east-1"
}



variable "root_zone" {
  type = string
  description = "the root domain name, if different from domain_name"
  default = null
}

variable "domain_name" {
  type = string
  description = "domain name where the website will be hosted"
}

variable "keybase_user" {
  type = string
  description = "keybase username to use to encrypt IAM secret access keys"
  default = null
}

variable "tags" {
  type = map(string)
  description = "tags to be applied to every resource"
}



output "cloudfront_id" {
  description = "distribution ID of the cloudfront that was created"
  value = module.cloudfront.distribution_id
}

output "s3_uri" {
  description = "URI of the S3 bucket that was created"
  value = "s3://${module.bucket.bucket_name}"
}

output "gitlab_access_key_id" {
  description = "the gitlab user's access key id"
  value = module.gitlab-deploy-user.access_key_id
}

output "gitlab_secret_access_key" {
  description = "the gitlab user's access key id"
  value = module.gitlab-deploy-user.secret_access_key
}



data "aws_route53_zone" "zone" {
  name = var.root_zone == null ? var.domain_name : var.root_zone
}



module "bucket" {
  source = "./bucket"

  name = var.domain_name
  tags = var.tags
}



module "tls" {
  source = "./tls"

  domain_name = var.domain_name
  zone_id = data.aws_route53_zone.zone.zone_id
  tags = var.tags
}



module "cloudfront" {
  source = "./cloudfront"

  bucket_arn = module.bucket.bucket_arn
  bucket_name = module.bucket.bucket_name
  bucket_regional_domain_name = module.bucket.bucket_regional_domain_name
  certificate_arn = module.tls.arn
  domain_name = var.domain_name
  zone_id = data.aws_route53_zone.zone.zone_id
  tags = var.tags
}



data "aws_s3_bucket" "terraform_state" {
  bucket = "terraform-francisferrell"
}

data "aws_iam_policy_document" "terraform_state_access" {
  statement {
    actions = [
      "s3:GetObject",
    ]

    resources = [
      "${data.aws_s3_bucket.terraform_state.arn}/francisferrell.dev.tfstate",
    ]
  }
}

resource "aws_iam_policy" "terraform_state_policy" {
  name = "${data.aws_s3_bucket.terraform_state.id}-francisferrell.dev.tfstate-read-access"
  description = "grants read access to s3://${data.aws_s3_bucket.terraform_state.id}/francisferrell.dev.tfstate"
  policy = data.aws_iam_policy_document.terraform_state_access.json
  tags = var.tags
}

module "gitlab-deploy-user" {
  source = "./api-user"

  name = "gitlab-${var.domain_name}"
  keybase_user = var.keybase_user
  policy_arns = [
    aws_iam_policy.terraform_state_policy.arn,
    module.bucket.read_write_policy_arn,
    module.cloudfront.invalidation_policy_arn,
  ]
  tags = var.tags
}

